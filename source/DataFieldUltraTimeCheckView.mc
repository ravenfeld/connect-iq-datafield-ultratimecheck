using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;

class DataFieldUltraTimeCheckView extends WatchUi.DataField {

    private var nameNextWaypoint;
    private var timeCheckNextWaypoint; 
    private var restTimeNextWaypoint;
    
    private var background=Graphics.COLOR_TRANSPARENT;
    private var canShowAlert=false;
    
    private var info = WatchUi.loadResource(Rez.Strings.Info);
    private var nothing = WatchUi.loadResource(Rez.Strings.Nothing);
    private var name = WatchUi.loadResource(Rez.Strings.Name);
    private var tb = WatchUi.loadResource(Rez.Strings.TB);
        
    function initialize() {
        DataField.initialize();
    }
    
    function compute(info) {   
    	if(info.nameOfNextPoint!=null){    	
    		parseString(info.nameOfNextPoint);
    		if(timeCheckNextWaypoint!=null){
    			var time = timeCheckNextWaypoint-info.elapsedTime;
    			if(time>0){     		
					restTimeNextWaypoint = timeToString(timeCheckNextWaypoint-info.elapsedTime);
				}else{
					if(WatchUi.DataField has :showAlert && !canShowAlert ){
						showAlert(new DataFieldUltraTimeCheckAlertView());
						canShowAlert=true;
					}
					restTimeNextWaypoint = "0:00";
				}
    		}
    	}
    }
    
	function onUpdate(dc){
			
		var color = (getBackgroundColor() == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
        dc.setColor( color, getBackgroundColor() );
        dc.clear();
        
		var flag = getObscurityFlags();
		
		if(flag==OBSCURE_BOTTOM+OBSCURE_TOP+OBSCURE_RIGHT+OBSCURE_LEFT){
        	background = Graphics.COLOR_TRANSPARENT;
        	var fonts = getFontsVertical(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.5,dc.getHeight()*0.3],
        			[dc.getWidth()*0.5,dc.getHeight()*0.55],
        			[dc.getWidth()*0.5,dc.getHeight()*0.3],
        			[dc.getWidth()*0.5,dc.getHeight()*0.55],
        			false);
        			
        	
        }else if(flag==OBSCURE_RIGHT+OBSCURE_LEFT+OBSCURE_BOTTOM){
        	background = Graphics.COLOR_RED;
        	var fonts = getFontsVertical(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];        	
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.5,dc.getHeight()*0.175],
        			[dc.getWidth()*0.5,dc.getHeight()*0.6],
        			[dc.getWidth()*0.5,dc.getHeight()*0.175],
        			[dc.getWidth()*0.5,dc.getHeight()*0.6],
        			false);
        			
        }else if(flag==OBSCURE_RIGHT+OBSCURE_LEFT+OBSCURE_TOP){
        	background = Graphics.COLOR_GREEN;
        	var fonts = getFontsVertical(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];        	
        	drawText(dc,
        			dc.getWidth()*0.8,
        			font_text,
        			font_number,
        			[dc.getWidth()*0.5,dc.getHeight()*0.25],
        			[dc.getWidth()*0.5,dc.getHeight()*0.7],
        			[dc.getWidth()*0.5,dc.getHeight()*0.25],
        			[dc.getWidth()*0.5,dc.getHeight()*0.7],
        			false);
        			
        	
        }else if(flag==OBSCURE_BOTTOM+OBSCURE_LEFT){
        	background = Graphics.COLOR_ORANGE;
      	    var fonts = getFontsVertical(dc,dc.getHeight()/2);
			var font_text = fonts[0];
        	var font_number = fonts[1];  
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.55,dc.getHeight()*0.15],
        			[dc.getWidth()*0.55,dc.getHeight()*0.4],
        			[dc.getWidth()*0.55,dc.getHeight()*0.15],
        			[dc.getWidth()*0.55,dc.getHeight()*0.4],
        			false);
        			
        	
        }else if(flag==OBSCURE_RIGHT+OBSCURE_BOTTOM){
        	background = Graphics.COLOR_PURPLE;
      	    var fonts = getFontsVertical(dc,dc.getHeight()/2);
			var font_text = fonts[0];
        	var font_number = fonts[1];  
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.45,dc.getHeight()*0.15],
        			[dc.getWidth()*0.45,dc.getHeight()*0.4],
        			[dc.getWidth()*0.45,dc.getHeight()*0.15],
        			[dc.getWidth()*0.45,dc.getHeight()*0.4],
        			false);
        			
        	
        }else if(flag==OBSCURE_TOP+OBSCURE_LEFT){
        	background = Graphics.COLOR_PINK;
      	    var fonts = getFontsVertical(dc,dc.getHeight()/2);
			var font_text = fonts[0];
        	var font_number = fonts[1];  
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.55,dc.getHeight()*0.55],
        			[dc.getWidth()*0.55,dc.getHeight()*0.8],
        			[dc.getWidth()*0.55,dc.getHeight()*0.55],
        			[dc.getWidth()*0.55,dc.getHeight()*0.8],
        			false);
        			
        	
        }else if(flag==OBSCURE_RIGHT+OBSCURE_TOP){
        	background = Graphics.COLOR_YELLOW;
      	    var fonts = getFontsVertical(dc,dc.getHeight()/2);
			var font_text = fonts[0];
        	var font_number = fonts[1];  
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.45,dc.getHeight()*0.55],
        			[dc.getWidth()*0.45,dc.getHeight()*0.8],
        			[dc.getWidth()*0.45,dc.getHeight()*0.55],
        			[dc.getWidth()*0.45,dc.getHeight()*0.8],
        			false);
        			
        	
        }else if(flag==OBSCURE_RIGHT+OBSCURE_LEFT){        	        
        	background = Graphics.COLOR_BLUE;
			var fonts = getFontsHorizontal(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];
        	var enable_title = true;
        	if(font_number == Graphics.FONT_SMALL){
        		font_number = Graphics.FONT_NUMBER_MILD;
        		enable_title = false;
        	}
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.25,dc.getHeight()*0.5],
        			[dc.getWidth()*0.75,dc.getHeight()*0.5],
        			[dc.getWidth()*0.25,dc.getHeight()*0.5],
        			[dc.getWidth()*0.75,dc.getHeight()*0.5],
        			enable_title);
        			
        	
        }else if(flag==OBSCURE_RIGHT){
            background = Graphics.COLOR_DK_BLUE;
        	var fonts = getFontsVertical(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];              	
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.45,dc.getHeight()*0.25],
        			[dc.getWidth()*0.45,dc.getHeight()*0.7],
        			[dc.getWidth()*0.45,dc.getHeight()*0.25],
        			[dc.getWidth()*0.45,dc.getHeight()*0.7],
        			false);
        			
        	
        }else if(flag==OBSCURE_LEFT){
        	background = Graphics.COLOR_DK_GREEN;
        	var fonts = getFontsVertical(dc,dc.getHeight());
			var font_text = fonts[0];
        	var font_number = fonts[1];        	
        	drawText(dc,
        			dc.getWidth(),
        			font_text,
        			font_number,
        			[dc.getWidth()*0.55,dc.getHeight()*0.25],
        			[dc.getWidth()*0.55,dc.getHeight()*0.7],
        			[dc.getWidth()*0.55,dc.getHeight()*0.25],
        			[dc.getWidth()*0.55,dc.getHeight()*0.7],
        			false);
        }
        		
	}

	function getFontsVertical(dc, height_max){
        var font_text = Graphics.FONT_LARGE;
        var font_number = Graphics.FONT_NUMBER_THAI_HOT;
        
        var text_height = dc.getTextDimensions("8",font_text)[1];
        var number_height = dc.getTextDimensions("8",font_number)[1];
        while(text_height+number_height>height_max){
        	if(font_number == Graphics.FONT_NUMBER_THAI_HOT){
        		font_text = Graphics.FONT_LARGE;
        		font_number = Graphics.FONT_NUMBER_HOT;
        	}else if(font_number == Graphics.FONT_NUMBER_HOT){
        		font_text = Graphics.FONT_MEDIUM;
        		font_number = Graphics.FONT_NUMBER_MEDIUM;
        	}else if(font_number == Graphics.FONT_NUMBER_MEDIUM){
        		font_text = Graphics.FONT_MEDIUM;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else if(font_number == Graphics.FONT_NUMBER_MILD && font_text == Graphics.FONT_MEDIUM){
        		font_text = Graphics.FONT_SMALL;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else if(font_text == Graphics.FONT_SMALL){
        		font_text = Graphics.FONT_TINY;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else{
        		font_text = Graphics.FONT_TINY;
        		font_number = Graphics.FONT_SMALL;
        	}
        	
        	if(font_number == Graphics.FONT_SMALL){
        		text_height = 0;
        		number_height = 0;
        	}else{
        		text_height = dc.getTextDimensions("8",font_text)[1];
        		number_height = dc.getTextDimensions("8",font_number)[1]*FontUtils.COEF_SIZE_TEXT_NUMBER;
        		
        	}        	
        }
        return [font_text,font_number];	
	}

	function getFontsHorizontal(dc,height_max){
        var font_text = Graphics.FONT_MEDIUM;
        var font_number = Graphics.FONT_NUMBER_MEDIUM;
        
        var text_small_height = dc.getTextDimensions("8",Graphics.FONT_TINY)[1];
        var number_height = dc.getTextDimensions("8",font_number)[1]*FontUtils.COEF_SIZE_TEXT_NUMBER;
        while(number_height+text_small_height>height_max){
        	if(font_number == Graphics.FONT_NUMBER_MEDIUM){
        		font_text = Graphics.FONT_MEDIUM;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else if(font_number == Graphics.FONT_NUMBER_MILD && font_text == Graphics.FONT_MEDIUM){
        		font_text = Graphics.FONT_SMALL;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else if(font_text == Graphics.FONT_SMALL){
        		font_text = Graphics.FONT_TINY;
        		font_number = Graphics.FONT_NUMBER_MILD;
        	}else{
        		font_text = Graphics.FONT_TINY;
        		font_number = Graphics.FONT_SMALL;
        	}
        	
        	if(font_number == Graphics.FONT_SMALL){
        		number_height = 0;
        	}else{
        		number_height = dc.getTextDimensions("8",font_number)[1]*FontUtils.COEF_SIZE_TEXT_NUMBER;
        	}	
        }
        return [font_text,font_number];	
	}
					
	function drawText(dc,width_max,font_text,font_number,p_nothing,p_info,p_name,p_time,enable_title){
		var color = (getBackgroundColor() == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
		
        dc.setColor( color, Graphics.COLOR_TRANSPARENT );
		//dc.setColor( background, Graphics.COLOR_TRANSPARENT );
        if(nameNextWaypoint==null || nameNextWaypoint.length()==0){
			dc.drawText(p_nothing[0] , p_nothing[1], font_text,	nothing,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
			dc.drawText(p_info[0] , p_info[1], font_text, info,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
		}else{
			cutNameNextWaypoint(dc,width_max,font_text);
			
			var x_name = p_name[0];
			var y_name = p_name[1];
			var x_time = p_time[0];
			var y_time = p_time[1];
			
			if(enable_title){
				
				var space=dc.getHeight()*0.2;
				
				dc.drawText(p_name[0] , p_name[1]-space*1.6, Graphics.FONT_XTINY, name,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
				dc.drawText(p_time[0] , p_time[1]-space*1.6, Graphics.FONT_XTINY, tb,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
				
				y_name = p_name[1]+space*0.7;
				y_time = p_time[1]+space*0.7;
			}
				
			dc.drawText(x_name , y_name, font_text, nameNextWaypoint,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
			if(timeCheckNextWaypoint!=null){
				if(restTimeNextWaypoint.equals("0:00")){
					dc.setColor( Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT );
				}
				dc.drawText(x_time , y_time, font_number, restTimeNextWaypoint,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
			}else{
				dc.drawText(x_time , y_time, font_text, nothing,Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
			}
			/*
			var height_1 =dc.getTextDimensions("8",font_number)[1]*FontUtils.COEF_SIZE_TEXT_NUMBER;
			dc.drawRectangle(0 , y_time-height_1/2,1000,height_1);
			var height_2 =dc.getTextDimensions("8",font_text)[1];
			dc.drawRectangle(0 , y_name-height_2/2,1000,height_2);	*/
			
		}	
		
	}
	
	function stringToTime(timeString){
		var time =0;	
		var separatorIndex;

		separatorIndex = timeString.find(":");
		if(separatorIndex!=null){
			try {
				var number_1 = timeString.substring(0, separatorIndex).toNumber();
				if( number_1 == null ) {
					number_1 = 0;
				} 
				var number_2 = timeString.substring(separatorIndex+1,timeString.length()).toNumber();
				if( number_2 == null ) {
					number_2 = 0;
				}
				time = (number_1*60*60+number_2*60)*1000;
		 	} catch( ex instanceof UnexpectedTypeException ) { 
		  	}
			
		}else {
			try { 
				var number = timeString.toNumber();
				if( number == null ) {
					number = 0;
				}
				time = (number*60*60)*1000;
		 	} catch( ex instanceof UnexpectedTypeException ) {			 	
		  	}
		}
		
		if(time == 0){
			return null; 	
		}else{
			return time;
		}
	}
	
	function parseString(text){
		var separatorIndex = text.find(";");
		if(separatorIndex!=null){
			 nameNextWaypoint = trim(text.substring(0, separatorIndex));
			 timeCheckNextWaypoint = stringToTime(trim(text.substring(separatorIndex+1,text.length())));
		}else{
			nameNextWaypoint = trim(text);
			timeCheckNextWaypoint = null;
		}
	}
	
	function timeToString(long){
		var seconds = long/1000 % 60;
		var minutes = (long /1000 / 60) % 60;
		var hour = long/1000/60/60;
		if(hour>0){
			return hour+":"+minutes.format("%02d");
		}else{
			return minutes+":"+seconds.format("%02d");
		}
	}
	
	function trim(text){
		var separatorIndex = text.find(" ");
		while(separatorIndex==0){
			text = text.substring(0, separatorIndex)+text.substring(separatorIndex+1, text.length());
			separatorIndex = text.find(" ");
		}
		
		if(text.length()>1){
			var last_char = text.substring(text.length()-1, text.length());
			while(last_char.equals(" ")){
				text = text.substring(0, text.length()-1);
				last_char = text.substring(text.length()-1, text.length());
			}
		}
		
		return text;
	}
	
	function cutNameNextWaypoint(dc,max_width,font_text){
		var text_width = dc.getTextDimensions(nameNextWaypoint,font_text)[0];
		var char_width = dc.getTextDimensions("8",font_text)[0];
		if(text_width>=max_width){
			var nbChar = max_width/char_width;
			nameNextWaypoint = nameNextWaypoint.substring(0,nbChar-3)+"...";
		}
	}
}

class DataFieldUltraTimeCheckAlertView extends WatchUi.DataFieldAlert {

	private var time_barrier_exceeded = WatchUi.loadResource(Rez.Strings.TimeBarrierExceeded);
	
	function initialize() {
        DataFieldAlert.initialize();
    }
    
    function onUpdate(dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);

        dc.drawText(dc.getWidth() / 2,
                    dc.getHeight() / 2,
                    Graphics.FONT_SMALL,
                    time_barrier_exceeded,
                    Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }
}